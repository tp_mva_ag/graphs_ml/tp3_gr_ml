from tqdm import tqdm
import scipy.io as sio
from src.helper import *


def iterative_hfs(nb_iter=10, filepath="/data/data_iterative_hfs_graph.mat"):
    """ A function performing the Iterative HFS on a test dataset

    :param nb_iter: number of iterations
    :return: labels: The computed labels
    :return: accuracy: The accuracy list over iterations
    """

    mat = sio.loadmat(filepath)
    w, y, y_masked = mat["W"], mat["Y"], mat["Y_masked"]

    classes = np.unique(y_masked[y_masked > 0])

    #####################################
    # Compute the initialization vector f #
    #####################################

    # f takes the values of y_marked and 0 otherwise
    f = y_masked.copy()
    f[f == 0] = np.zeros(np.sum(f == 0)) * 1.5

    #####################################
    #####################################

    #################################################################
    # compute the hfs solution, using iterated averaging            #
    # remember that column-wise slicing is cheap, row-wise          #
    # expensive and that W is already undirected                    #
    #################################################################

    accuracy_list = [0]

    id_iter = 0
    # We will iterate for the number of iteration required, and while there is still values without labels
    while (id_iter < nb_iter or np.sum(f <= 0.5) != 0) and accuracy_list[-1] != 1.0:
        id_iter += 1

        # We go through all the points, but randomly to avoid to label everything as '1' if
        # the '2' are at the end of the list
        rge = np.arange(len(y))
        np.random.shuffle(rge)

        for i in tqdm(rge, 'Iteration #%d over %d' % (id_iter, nb_iter)):
            # We re-label only the masked points.
            # Though, we could re-label them if there was uncertainty on their values.
            if y_masked[i] == 0:
                # We compute the sum of weights of the non-zero neighbours
                denominator = (f != 0).T * w.getcol(i)

                #  We verify we do not divide by 0
                if denominator != 0:
                    numerator = (f.T * w.getcol(i))
                    f[i] = numerator / denominator
                else:
                    f[i] = 0

        # We compute and print the accuracy
        labels = [classes[np.argmin(np.abs(classes - f[i]))] for i in range(len(y_masked))]
        accuracy = (labels == y.reshape(-1)).mean()
        accuracy_list.append(accuracy)

        print('#0 = %d | #1 = %d | #2 = %d | acc = %f' %
              (int(np.sum(f < 0.5)), int(np.sum((0.5 < f) * (f < 1.5))), int(np.sum(f >= 1.5)), accuracy))

    ################################################
    # Assign the label in {1,...,c}                #
    ################################################

    labels = [classes[np.argmin(np.abs(classes - f[i]))] for i in range(len(y_masked))]

    ################################################
    ################################################

    accuracy = (labels == y.reshape(-1)).mean()
    return labels, accuracy
