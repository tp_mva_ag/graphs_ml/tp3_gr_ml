from src.Online_SSL.online_SSL_func import create_user_profile, online_face_recognition
from src.Large_Scale_Label_Propagation.iterative_func import iterative_hfs
from src.helper import hardHFS, build_laplacian
import scipy.io as sio
from time import time

# profiles = ['Antonin', 'Valentine']
#
# for profile in profiles:
#     create_user_profile(profile)
#
# online_face_recognition(profiles, choosen_filter="box")

t0 = time()
_, accuracy_it = iterative_hfs(10)
iter_time = time() - t0

# Loading dataset
mat = sio.loadmat("data/data_iterative_hfs_graph.mat")
w, y, y_masked = mat["W"], mat["Y"], mat["Y_masked"]

t1 = time()
lapl = build_laplacian(w)
f= hardHFS(w, y_masked, laplacian=lapl, single_label=True)
hfs_time = time() - t1

print("Accuracy = %f" % accuracy_it)
print("Time HFS = %f | TIme_Iter = %f" % (0, iter_time))
